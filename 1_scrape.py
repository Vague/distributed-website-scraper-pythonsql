#import pdb

from selenium import webdriver
from time import sleep
import random
from bs4 import BeautifulSoup

def scrape_arts(page_html):
    print("scrape_arts")
    #print("bs")
    soup = BeautifulSoup(page_html, 'html.parser')
    # find all links in the HTML
    links = soup.find_all('a')
    artist_links = []
    for link in links:
        href = link.get('href')
        if href is not None and '/sets/' not in href and len(href) > 1 :
            if href[0] == '/' and 'tags' not in href and len(href.split('/')) == 2:
                artist_link = f'specific_website{href}'
                artist_links.append(artist_link)
            
    #arts = ','.join(sorted(set(artist_links)))
    #return arts
    return artist_links


browser = webdriver.Firefox()
browser.get('specific_website_link')
assert 'websitename' in browser.title

#elem = browser.find_element(By.NAME, 'p')  # Find the search box
#elem.send_keys('seleniumhq' + Keys.RETURN)
# https://stackoverflow.com/questions/72773206/selenium-python-attributeerror-webdriver-object-has-no-attribute-find-el
#elem = browser.find_element("xpath", "'paging-eof sc-border-light-top'") 
#elem = browser.find_element("xpath", "//div[@class='paging-eof sc-border-light-top']")
#browser.execute_script("window.scrollBy(0,document.body.scrollHeight)")
#driver.find_element_by_xpath("//div[@class='fc-day-content' and text()='15']")
#browser.find_element_by_class_name("paging-eof sc-border-light-top")
#browser.find_element_by_xpath("//div[@class='paging-eof sc-border-light-top' ")

print("beginning scrape")
sleep(1)
while True:
    try:
        elem = browser.find_element("xpath", "//div[@class='paging-eof sc-border-light-top']")
        browser.execute_script("window.scrollBy(0,document.body.scrollHeight)")
        sleep(5)
        print("scrolling done")
        break;
    except:
        print("exception")
        browser.execute_script("window.scrollBy(0,document.body.scrollHeight)")
        tsleep=random.randrange(1,200)/100
        sleep(tsleep)

print("scrolling done")
        
page_html= browser.page_source.encode("utf-8")
arts = scrape_arts(page_html)
print("got " + str(arts))
browser.quit()

#print("write to csv for loading into db")
## https://stackoverflow.com/questions/14037540/writing-a-python-list-of-lists-to-a-csv-file
## https://stackoverflow.com/questions/4706499/how-do-i-append-to-a-file
## python is kinda a garbage language
#with open("artists.csv", "a") as f:
#    for a in arts:
#        f.write(a)



import sqlite3
connection = sqlite3.connect("artists.db")
cursor = connection.cursor()
# create tables
cursor.execute("CREATE TABLE IF NOT EXISTS artists ( name TEXT primary key, claimed text, flags text)")
#cursor.execute("CREATE TABLE IF NOT EXISTS songs ( artist TEXT , song TEXT primary key, flags TEXT, claimed text, timestamp TEXT, counter TEXT)")
#cursor.execute("CREATE TABLE IF NOT EXISTS threads ( pid TEXT primary key, startdate text, checkin text, comment text)")

print(arts)
for art in arts:
    try:
        print(art)
        reject_list = ['/discover', '/terms-of-use','/imprint', '/feed', '/upload' ]
        skip = False
        for rej in reject_list:
            if rej in art:
                print("skip")
                skip = True
                continue # this is not art
        if skip == False:
            query = "INSERT INTO artists ( name, claimed ) values (?,?)"
            cursor.execute(query, (art,"false"))
            connection.commit()
    except:
        pass

