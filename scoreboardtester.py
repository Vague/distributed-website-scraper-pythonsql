import scoreboarddb

import time

host = 'myhost'
number_of_total_songs = 1000
number_of_complete_songs = 500
current_song = 'darude_sandstorm'
artist = 'darude'
status = 'indev'
print("--------------------")
print("register host")
scoreboarddb.register(host)
print("--------------------")
time.sleep(1)

print("--------------------")
print("checkin host")
print(scoreboarddb.checkin(host))
print("--------------------")
time.sleep(1)

print("--------------------")
print("get last checkin")
print(scoreboarddb.get_last_checkin_time(host))
print("--------------------")

print("--------------------")
print("checkin")
print('check in timestamp: ' + scoreboarddb.checkin(host))
print("--------------------")

print("--------------------")
print("get last checkin")
print(scoreboarddb.get_last_checkin_time(host))
print("--------------------")

print("--------------------")
print("get host dict")
print(scoreboarddb.get_host_dict(host))
print("--------------------")

print("--------------------")
print('set number of total songs: ' + scoreboarddb.set_number_of_total_songs(host, number_of_total_songs ))
print("--------------------")

print("--------------------")
print('get number of total songs: ' + scoreboarddb.get_number_of_total_songs(host ))
print("--------------------")
 
print("--------------------")
print('set number of complete songs: ' + scoreboarddb.set_number_of_complete_songs(host, number_of_complete_songs ))
print("--------------------")

print("--------------------")
print('get number of complete songs: ' + scoreboarddb.get_number_of_complete_songs(host ))
print("--------------------")

print("--------------------")
print('set current_song')
result = scoreboarddb.set_current_song(host,current_song)
print(result)
print("--------------------")

print("--------------------")
print('get current_song')
ans = current_song
result = scoreboarddb.get_current_song(host)
if ans == result:
    print('got expected answer ', result)
else: 
    print('got unexpected answer ', result)
print("--------------------")

print("--------------------")
print('set artist')
result = scoreboarddb.set_artist(host,artist)
print(result)
print("--------------------")

print("--------------------")
print('get current_artist')
ans = artist
result = scoreboarddb.get_artist(host)
if ans == result:
    print('got expected answer ', result)
else: 
    print('got unexpected answer ', result)
print("--------------------")

print("--------------------")
print('set thread status ')
ans = status
result = scoreboarddb.set_thread_status(host,ans)
if ans == result:
    print('got expected answer ', result)
else: 
    print('got unexpected answer ', result)
print("--------------------")

print("--------------------")
print('get thread status ')
ans = status
result = scoreboarddb.get_thread_status(host)
if ans == result:
    print('got expected answer ', result)
else: 
    print('got unexpected answer ', result)
print("--------------------")

