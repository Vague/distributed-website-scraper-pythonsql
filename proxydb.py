
# read the artists db.

import sqlite3
import os # (for getpid)
connection = sqlite3.connect("proxy/proxy.db", check_same_thread=False)
#conn = sqlite3.connect('your.db', check_same_thread=False)
cursor = connection.cursor()


def get_next_unclaimed_host():
    # return string artist url
    res = cursor.execute("select host from proxy where userpid = 'na' limit 1;")
    res = res.fetchall()[0][0]
    #print(res)
    #print(type(res))
    return res

def claim_host(host):
    # return boolean for success
    print("claiming proxy " + host)
#sqlite> update artists set claimed = 'true' where name = 'site' ;
    res = cursor.execute("update proxy set userpid = '" + str(os.getpid())  + "' where host = '" + host + "';")
    connection.commit()

def unclaim_host(host):
    # return boolean for success
    print("unclaiming proxy " + host)
#sqlite> update artists set claimed = 'true' where name = 'site' ;
    res = cursor.execute("update proxy set userpid = 'na' where host = '" + host + "';")
    connection.commit()

def get_port_for_host(host):
    res = cursor.execute("select port from proxy where host = '" + host + "' ;")
    res = res.fetchall()[0][0]
    #print(res)
    #print(type(res))
    return res

def get_all_hosts():
    res = cursor.execute("select host from proxy;")
    res = res.fetchall()
    return_list = []
    for r in res:
        return_list.append(r[0])
    res = return_list
    #print(res)
    #print(type(res))
    return res
