from selenium import webdriver
from time import sleep
import random
from bs4 import BeautifulSoup

def scrape_arts(page_html):
    # this came from GPT4 in 20230329!!!!  from bs4 import BeautifulSoup
    # assume that the specificsite page HTML is stored in a variable called page_html
    # you can replace this with the actual code to dump the page HTML from Selenium
    # create a BeautifulSoup object from the page HTML
    #print("bs")
    soup = BeautifulSoup(page_html, 'html.parser')

    #print("links")
    # find all links in the HTML
    links = soup.find_all('a')

    artist_links = []
    for link in links:
        href = link.get('href')
        if href is not None and '/sets/' not in href and len(href) > 1 :
            if href[0] == '/' and 'tags' not in href and len(href.split('/')) == 2:
    #            print("'"+str(href)+"'")
                artist_link = f'specificsite'
                artist_links.append(artist_link)
            
    # print the links to the artist pages
    #for artist_link in artist_links:
        #print(artist_link)
    arts = ','.join(sorted(set(artist_links)))
    #print(arts)
    return arts



browser = webdriver.Firefox()
browser.get('specificsongurl')
assert 'site' in browser.title

#elem = browser.find_element(By.NAME, 'p')  # Find the search box
#elem.send_keys('seleniumhq' + Keys.RETURN)
# https://stackoverflow.com/questions/72773206/selenium-python-attributeerror-webdriver-object-has-no-attribute-find-el
# grrrrrr
#elem = browser.find_element("xpath", "'paging-eof sc-border-light-top'") 
#elem = browser.find_element("xpath", "//div[@class='paging-eof sc-border-light-top']")
#browser.execute_script("window.scrollBy(0,document.body.scrollHeight)")
#driver.find_element_by_xpath("//div[@class='fc-day-content' and text()='15']")
#browser.find_element_by_class_name("paging-eof sc-border-light-top")
#browser.find_element_by_xpath("//div[@class='paging-eof sc-border-light-top' ")

#print("scroll time")
sleep(1)
while True:
    #print("scrolling")
    try:
        elem = browser.find_element("xpath", "//div[@class='paging-eof sc-border-light-top']")
        browser.execute_script("window.scrollBy(0,document.body.scrollHeight)")
        break;
    except:
        browser.execute_script("window.scrollBy(0,document.body.scrollHeight)")
        tsleep=random.randrange(1,200)/100
        sleep(tsleep)
        

# https://stackoverflow.com/questions/28593438/how-to-print-page-source-with-selenium
page_html= browser.page_source.encode("utf-8")
#print(page_html)


try:
    print(scrape_arts(page_html))
except:
    pass

browser.quit()


