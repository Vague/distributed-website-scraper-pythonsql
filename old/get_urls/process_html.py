# this came from GPT4 in 20230329!!!!
from bs4 import BeautifulSoup

# assume that the site page HTML is stored in a variable called page_html
# you can replace this with the actual code to dump the page HTML from Selenium

# create a BeautifulSoup object from the page HTML
soup = BeautifulSoup(page_html, 'html.parser')

# find all links in the HTML
links = soup.find_all('a')

# loop through all links and check if they point to an artist's page
artist_links = []
for link in links:
    href = link.get('href')
    if href is not None and 'specificsite' in href and '/sets/' not in href:
        # the link points to a specificsite page and is not a link to a set
        parts = href.split('/')
        if len(parts) > 1:
            # the artist's name is the second part of the URL
            artist_name = parts[1]
            # construct the link to the artist's page
            artist_link = f'specificsite{artist_name}'
            artist_links.append(artist_link)

# print the links to the artist pages
for artist_link in artist_links:
    print(artist_link)

