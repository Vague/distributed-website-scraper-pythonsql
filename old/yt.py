#import sys # for sys.exit
#import subprocess

import json
import yt_dlp
import time # for sleep
#artist_url='specificsite'

print("songs mod")

def download(URLS):
    ydl_opts = {
    'format': 'aac/bestaudio/best',
    # ℹ️ See help(yt_dlp.postprocessor) for a list of available Postprocessors and their arguments
    'postprocessors': [{  # Extract audio using ffmpeg
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'aac',
    }]
    }
    # URLS takes a list of strings, or a single string. it probably takes more?
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        error_code = ydl.download(URLS)

def get_songs( artist_url ):
    # returns list of strings = song urls

    songs = []
    print("get songs from " + artist_url)
    #  See help(yt_dlp.YoutubeDL) for a list of available options and public functions
    ydl_opts = {}
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(artist_url, download=False)

        # ℹ️ ydl.sanitize_info makes the info json-serializable
        res = json.dumps(ydl.sanitize_info(info))
        print(res)
    json_result = json.loads(res)
    for item in json_result['entries']:
        song = item['webpage_url']
        songs.append(song)

    return songs



def proxy_get_songs( artist_url, proxy ):
    # returns list of strings = song urls

    #  See help(yt_dlp.YoutubeDL) for a list of available options and public functions
    ydl_opts = {
    'proxy':proxy,
    'ignoreerrors':'true',
    }

    songs = []
    print("get songs from " + artist_url)
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(artist_url, download=False)

        # ℹ️ ydl.sanitize_info makes the info json-serializable
        res = json.dumps(ydl.sanitize_info(info))
        print(res)
    json_result = json.loads(res)
    for item in json_result['entries']:
        song = item['webpage_url']
        songs.append(song)

    return songs

def proxy_download(URLS,proxy):
    # ratelimit is in bytes, 2000 comes out as 2KiB/s
    ydl_opts = {
    'ratelimit' : 200000,
    'format': 'aac/bestaudio/best',
    'proxy':proxy,
    # ℹ️ See help(yt_dlp.postprocessor) for a list of available Postprocessors and their arguments
    'postprocessors': [{  # Extract audio using ffmpeg
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'aac',
    }]
    }
    # URLS takes a list of strings, or a single string. it probably takes more?
    
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        error_code = ydl.download(URLS)


# dont be unrelenting, we need to relent and come back
# its the persistence that kills
#def proxy_download_unrelenting(URLS,proxy):
#    ydl_opts = {
#    'format': 'aac/bestaudio/best',
#    'proxy':proxy,
#    # ℹ️ See help(yt_dlp.postprocessor) for a list of available Postprocessors and their arguments
#    'postprocessors': [{  # Extract audio using ffmpeg
#        'key': 'FFmpegExtractAudio',
#        'preferredcodec': 'aac',
#    }]
#    }
#    # URLS takes a list of strings, or a single string. it probably takes more?
#    done = False
#    while not done:
#        try:
#            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
#                error_code = ydl.download(URLS)
#            done = True
#        except Exception as e:
#                print('proxy_download_unrelenting exception: ', e )
#                time.sleep(3000)
#
#
#
