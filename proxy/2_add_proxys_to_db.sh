# vague
# maintain a bunch of proxies for the python thing to use
# https://stackoverflow.com/questions/3112687/how-to-iterate-over-associative-arrays-in-bash


rm proxy.db -v 
sqlite3 proxy.db "CREATE TABLE IF NOT EXISTS proxy ( sshpid TEXT , port TEXT primary key, userpid TEXT, host TEXT);"
#  		sshpid port userpid host
declare -A JOB_LIST
#JOB_LIST[ssh1]="na 12000 na host1"
JOB_LIST[ssh2]="na 12001 na host2"
JOB_LIST[ssh3]="na 12002 na host3"
#JOB_LIST[ssh4]="na 12003 na host4"
#JOB_LIST[ssh5]="na 12004 na host5" # 🫡  unstable connection
JOB_LIST[ssh6]="na 12005 na host6"
JOB_LIST[ssh7]="na 12006 na host7"

for job in "${!JOB_LIST[@]}"; do
	echo key: $job, value: "${JOB_LIST[$job]}"
	arg=(${JOB_LIST[$job]})
	echo "'${arg[0]}'  , '${arg[1]}', '${arg[2]}', '${arg[3]}'"
	sqlite3 proxy.db "insert into proxy( sshpid , port , userpid, host ) values ( '${arg[0]}'  , '${arg[1]}', '${arg[2]}', '${arg[3]}' ) ; "
	sleep 0.25
done

sqlite3 proxy.db 'select * from proxy order by port ; '
sleep 0.25

max_conns(){ sqlite3 proxy.db "select port from proxy"  | wc -l ; }
num_jobs_bg(){ jobs -p | wc -l ; }


socks(){
	echo socks with $@
	sshpid=$1
	port=$2
	userpid=$3
	host=$4

	echo --- $'\n'getting $sshpid,$port,$userpid,$host
	echo ssh -D $port -C -N $host -o ServerAliveInterval=10 -o ServerAliveCountMax=4 -o ExitOnForwardFailure=yes & 
	ssh -D $port -C -N $host -o ServerAliveInterval=10 -o ServerAliveCountMax=4 -o ExitOnForwardFailure=yes & 
	sshpid=$!
	echo forked with sshpid $sshpid	
	sleep 1
	read -p 'continue> '
	sqlite3 proxy.db "update proxy set sshpid = '$sshpid' where port=$port"
	sleep 1
	wait
	sleep 1
	sqlite3 proxy.db "update proxy set sshpid = 'na' where port=$port"
	sleep 1
}

# spin up threads to maintain socks
while sleep 1 ; do
	while read sshpid port userpid host ; do
		# threads that hold the forked ssh proxies
		socks "$sshpid" "$port" "$userpid" "$host" &
		echo checking $(num_jobs_bg) against $(max_conns)
		echo ===
		sleep 1
	done < <( sqlite3 proxy.db "select * from proxy where sshpid = 'na' ;" -separator ' ' )
	sleep 1

	# blocks until we need a new proxy
	while [ "$(num_jobs_bg)" -ge "$(max_conns)" ] ; do
		# this cant handle deaths of the threads holding the forked ssh's. 
		# it handles death of the ssh process perfectly fine
		# we are having network issues not threads dying issues though so it should be fine
		echo have $(num_jobs_bg) of $(max_conns)
		#jobs -p
		sleep 2
		wait -n
	done
	echo going back around
	sleep 2
done

