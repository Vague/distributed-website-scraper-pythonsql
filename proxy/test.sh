#curl --socks5-hostname localhost:12002 ifconfig.me ; echo
echo local machine: $(curl 2>/dev/null ifconfig.me ; echo)

while read sshpid port userpid host ; do
	# threads that hold the forked ssh proxies
	echo ===
	echo "$sshpid" "$port" "$userpid" "$host" $(curl 2>/dev/null --socks5-hostname localhost:$port ifconfig.me ; echo) 
	sleep 0.25
done < <( sqlite3 proxy.db "select * from proxy where sshpid not like 'na';" -separator ' ' )
