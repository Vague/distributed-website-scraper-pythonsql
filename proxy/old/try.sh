# vague
# maintain a bunch of proxies for the python thing to use
# https://stackoverflow.com/questions/3112687/how-to-iterate-over-associative-arrays-in-bash

rm proxy.db -v 
sleep 1
sqlite3 proxy.db "CREATE TABLE IF NOT EXISTS proxy ( sshpid TEXT , port TEXT primary key, userpid TEXT, host TEXT, flags TEXT);"
sleep 1
#  		sshpid port userpid host
declare -A JOB_LIST
JOB_LIST[ssh1]="na 12000 na host1 none"
JOB_LIST[ssh2]="na 12001 na host2 none"
JOB_LIST[ssh3]="na 12002 na host3 none"
JOB_LIST[ssh4]="na 00000 na host4 none"

for job in "${!JOB_LIST[@]}"; do
	echo key: $job, value: "${JOB_LIST[$job]}"
	arg=(${JOB_LIST[$job]})
	echo "'${arg[0]}'  , '${arg[1]}', '${arg[2]}', '${arg[3]}', '${arg[4]}'"
	sqlite3 proxy.db "insert into proxy( sshpid , port , userpid, host, flags) values ( '${arg[0]}'  , '${arg[1]}', '${arg[2]}', '${arg[3]}', '${arg[4]}' ) ; "
done

sqlite3 proxy.db 'select * from proxy order by port ; '

max_conns(){ echo $(( $(sqlite3 proxy.db "select port from proxy"  | wc -l ) - 1  )) ; }
num_jobs_bg(){ jobs -p | wc -l ; }

# spin up threads to maintain socks
while sleep 1 ; do
	while read sshpid port userpid host flags; do
		# threads that hold the forked ssh proxies
		(
		echo --- $'\n'getting $sshpid,$port,$userpid,$host,$flags
		# check if we're the sacraficial thread 
		# kill the sacraficial thread to recheck db
		if [ "$host" = "blank" ] ; then 
			echo 'sleep 9999h &'
			sleep 9999h &
			sshpid=$!
		else
			echo 'ssh -D $port -C -N $host -o ServerAliveInterval=10 -o ServerAliveCountMax=4 -o ExitOnForwardFailure=yes & '
		       	ssh -D $port -C -N $host -o ServerAliveInterval=10 -o ServerAliveCountMax=4 -o ExitOnForwardFailure=yes & 
			sshpid=$!
		fi
		echo forked with sshpid $sshpid	
		sqlite3 proxy.db "update proxy set sshpid = '$sshpid' where port='$port'" 
		wait
		sqlite3 proxy.db "update proxy set sshpid = 'na' where port=$port"
		) &
		echo checking $(num_jobs_bg) against $(max_conns)
		echo ===
	done < <( sqlite3 proxy.db "select * from proxy where sshpid = 'na' ;" -separator ' ' )

	# blocks until we need a new proxy
	while [ "$(num_jobs_bg)" -ge "$(max_conns)" ] ; do
		# this cant handle deaths of the threads holding the forked sshd's. 
		# it handles death of the ssh process perfectly fine
		# we are having network issues not threads dying issues though so it should be fine
		echo have $(num_jobs_bg) against $(max_conns)
		#jobs -p
		sleep 2
		sqlite3 proxy.db 'select * from proxy order by port ; '
		wait -n
	done
	echo going back around
done

wait
