
# read the artists db.

import sqlite3
import json # for flags
import time
connection = sqlite3.connect("songs.db", check_same_thread=False)
cursor = connection.cursor()

##############################################################################################
# songs
##############################################################################################

cursor.execute("CREATE TABLE IF NOT EXISTS songs ( artist TEXT , song TEXT primary key, flags TEXT, claimed text, timestamp TEXT, counter TEXT, downloaded TEXT)")


# cursor.execute("CREATE TABLE IF NOT EXISTS songs ( artist TEXT , song TEXT primary key, flags TEXT, claimed text, timestamp TEXT, counter TEXT, downloaded TEXT)")

def get_number_of_downloaded_songs_from_artist(artist):
        res = cursor.execute("select count(*) from songs where artist = '" + artist + "' and flags = 'downloaded' ; ")
        res = res.fetchall()[0][0]
        return res

def get_song_dict(song):
        res = cursor.execute("select * from songs where song='"+song+"' ;")
        print("select * from songs where song='"+song+"' ;")
        res = res.fetchall()[0]
        d = {}
        d['artist'] = res[0]
        d['song'] = res[1]
        d['flags'] = res[2]
        d['claimed'] = res[3]
        d['timestamp'] = res[4]
        d['counter'] = res[5]
        d['downloaded'] = res[6]
        return d

def insert_song_metadata(artist, song):
    try:
        query = "INSERT INTO songs ( artist , song, flags, claimed, timestamp, counter, downloaded ) values (?,?,?,?,?,?,?)"
        flags = {}
        flags = json.dumps(flags)
        cursor.execute(query, (artist,song,flags,"false","0", "0", "false"))
        connection.commit()
        print("insert_song_metadata succeeded for " + artist + ", " + song)
        return True
    except Exception as e:
        print("exception in insert_song_metadata: " + str(e))
        return False

def get_all_songs():
    # return string artist url
    try:
        res = cursor.execute("select song from songs ;")
        res = res.fetchall()
        return_list = []
        for r in res:
            return_list.append(r[0])
        res = return_list
        #print(res)
        #print(type(res))
        return res
    except Exception as e:
        print("exception in insert_song_metadata: " + str(e))
        return False

def get_all_songs_by_artist(artist):
    # return string artist url
    try:
        res = cursor.execute("select song from songs where artist = '" + artist + "' ;")
        res = res.fetchall()
        return_list = []
        for r in res:
            return_list.append(r[0])
        res = return_list
        #print(res)
        #print(type(res))
        return res
    except Exception as e:
        print("exception in insert_song_metadata: " + str(e))
        return False

def get_next_undownloaded_song(artist):
    # return string artist url
    try:
        res = cursor.execute("select song from songs where artist='"+artist+"' and flags = 'undownloaded' limit 1 ;")
        res = res.fetchall()[0][0]
        #print(res)
        #print(type(res))
        return res
    except Exception as e:
        print("exception in get_next_undownloaded_song: " + str(e))
        return False

def get_all_undownloaded_songs(artist):
    try:
        res = cursor.execute("select song from songs where artist='"+artist+"' and flags = 'undownloaded' ;")
        res = res.fetchall()
        return_list = []
        for r in res:
            return_list.append(r[0])
        res = return_list
        #print(res)
        #print(res)
        #print(type(res))
        return res
    except Exception as e:
        print("exception in get_all_undownloaded_songs: " + str(e))
        return False

#sqlite> update artists set claimed = 'true' where name = 'artist' ;
def mark_song_downloaded(song):
        res = cursor.execute("update songs set flags = 'downloaded' where song='"+song+"' ;")
        connection.commit()
def mark_song_undownloaded(song):
        res = cursor.execute("update songs set flags = 'undownloaded' where song='"+song+"' ;")
        connection.commit()

def increment_song_counter(song):
        res = cursor.execute("select counter from songs where song='"+song+"' ;")
        res = res.fetchall()
        increment = str(int(res[0][0]) + 1)
        res = cursor.execute("update songs set counter = " + increment + " where song='"+song+"' ;")
        connection.commit()

def decrement_song_counter(song):
        res = cursor.execute("select counter from songs where song='"+song+"' ;")
        res = res.fetchall()
        decrement = str(int(res[0][0]) - 1)
        res = cursor.execute("update songs set counter = " + decrement + " where song='"+song+"' ;")
        connection.commit()
def decrement_song_counter(song):
        res = cursor.execute("select counter from songs where song='"+song+"' ;")
        res = res.fetchall()
        decrement = str(int(res[0][0]) - 1)
        res = cursor.execute("update songs set counter = " + decrement + " where song='"+song+"' ;")
        connection.commit()

def set_song_timestamp(song):
        res = cursor.execute("update songs set timestamp = " + str(int(time.time())) + " where song='"+song+"' ;")
        connection.commit()

def get_song_timestamp(song):
        res = cursor.execute("select timestamp from songs where song='" + song + "' ;" )
        res = res.fetchall()
        return res[0][0]

def get_song_timestamp_age(song):
    return int(time.time()) - int(get_song_timestamp(song))

def is_song_timestamp_old(song):
    age = get_song_timestamp_age(song)
    if age < (60*60*6):
        # false if less than 6h
        return False
#    if age == 0:
#        # false if no timestamp
#        return False
    # true if no timestamp (age = current unix time)
    if age > (60*60*6):
        # true if older than 6h
        return True




# TODO the song flag field should be a dictionary like it is for artists. 
# this change should be invisible, so i can do it later (or not at all)
