
# read the artists db.

import sqlite3
import json # for flags
connection = sqlite3.connect("artists.db", check_same_thread=False)
cursor = connection.cursor()
cursor.execute("CREATE TABLE IF NOT EXISTS artists ( name TEXT primary key, claimed text, flags text)")

##############################################################################################
# artists
##############################################################################################

def get_all_artists():
    # create tables
    #cursor.execute("CREATE TABLE IF NOT EXISTS songs ( artistname TEXT , songname TEXT primary key, visited INTEGER default 0, claimed text)")
    #cursor.execute("CREATE TABLE IF NOT EXISTS threads ( pid TEXT primary key, startdate text, checkin text, comment text)")

    # https://docs.python.org/3/library/sqlite3.html
    # we arent writing so we dont have to claim anything yet
    res = cursor.execute("select * from artists;")
    res = res.fetchall()
    return_list = []
    for r in res:
        return_list.append(r[0])
    res = return_list
    #print(res)
    #print(type(res))
    return res

def claim_artist(artist):
    # return boolean for success
    print("claiming artist " + artist)
#sqlite> update artists set claimed = 'true' where name = 'artisturl' ;
    res = cursor.execute("update artists set claimed = 'true' where name = '" + artist + "';")
# TODO 'claiming by "true" is nonsense! claim using pid or something
    connection.commit()

def unclaim_artist(artist):
    # return boolean for success
    print("unclaiming artist " + artist)
#sqlite> update artists set claimed = 'true' where name = 'artisturl' ;
    res = cursor.execute("update artists set claimed = 'false' where name = '" + artist + "';")
    connection.commit()

# sqlite> select name from artists where claimed = 'false' limit 1;
def get_artist_claim(artist):
    #select claimed from artists where name = 'artisturl' ;
    res = cursor.execute("select claimed from artists where name = '" + artist + "';" )
    res = res.fetchall()[0][0]
    #print(res)
    #type(res)
    if res == "false" :
        return False
    else:
        return True

# sqlite> select name from artists where claimed = 'false' limit 1;
def get_next_unclaimed_artist():
    # return string artist url
    res = cursor.execute("select name from artists where claimed = 'false' limit 1;")
    res = res.fetchall()[0][0]
    #print(res)
    #print(type(res))
    return res

def get_undownloaded_unmarked_artists():
    #qlite> select name from artists where flags like '%"downloaded": false%' and claimed = 'false';
    # https://docs.python.org/3/library/sqlite3.html
    res = cursor.execute("select name from artists where flags like '%\"downloaded\": false%' and claimed = 'false'; " )
    res = res.fetchall()
    return_list = []
    for r in res:
        return_list.append(r[0])
    res = return_list
    return res


def get_artist_flags(artist):
    #select claimed from artists where name = 'artisturl' ;
    res = cursor.execute("select flags from artists where name = '" + artist + "';" )
    res = res.fetchall()[0][0]
    #print(res)
    #type(res)
    return json.loads(res)

def set_artist_flags(artist,flags):
    # return boolean for success
#sqlite> update artists set claimed = 'true' where name = 'artisturl' ;
    flags = json.dumps(flags)
    res = cursor.execute("update artists set flags = '" + flags +"' where name = '" + artist + "';")
    connection.commit()

def mark_artist_downloaded(artist):
    flags = get_artist_flags(artist)
    flags['downloaded'] = True
    set_artist_flags(artist, flags)

def mark_artist_undownloaded(artist):
    flags = get_artist_flags(artist)
    flags['downloaded'] = False
    set_artist_flags(artist, flags)

def get_artist_downloaded(artist):
    flags = get_artist_flags(artist)
    return flags['downloaded']
