# distributed website-scraper-pythonsql

The presentation version of distributed scraper for a specific website. All references to the specific site have been removed and the system is likely nonfunctional in this state. The functional version exists somewhere else, but it hasnt been used in some time due to not having enough free space. I plan to redo this differently. I think it could be made so much simpler. 

Stages are manually started. The first stages scrapes the site and gets lists of artist and song urls. The next stages handle distributed downloading urls from several hosts using SOCKS5 proxies.

It connects to hosts over ssh to use as proxies for the download. It uses python3 and sqlite, selenium and beautifulsoup.


