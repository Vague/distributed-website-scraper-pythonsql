# get all songs from all artists and put them into a huge songs table
# but do it with threads

############################################################################################################
# setup script
############################################################################################################

import yt,proxydb
import artistsdb
import songsdb
import os,time
import threading
import scoreboarddb

############################################################################################################
# functions
############################################################################################################

def get_undownloaded_songs(art):
    time.sleep(0.05)
    songs = songsdb.get_all_undownloaded_songs(art)
    if len(songs) == 0:
        print("there are no valid songs, fetching...")
        #songs = yt.get_songs(art)
        proxy_string = 'socks5://127.0.0.1:'+proxydb.get_port_for_host(myhost)
        songs = yt.proxy_get_songs(art, proxy_string)
        for song in songs:
            time.sleep(0.05)
            mutex.acquire(1)
            songsdb.insert_song_metadata(art, song)
            songsdb.mark_song_undownloaded(song)
            mutex.release()
    return songs

def downloader(myhost, art):
    print("☠️  ☠️  ☠️  starting download !!! ")
    print("artist: " , art)
    print("host: " , myhost)

    time.sleep(0.05)
    mutex.acquire(1)
    artistsdb.claim_artist(art)
    mutex.release()

    scoreboarddb.set_artist(myhost, art)
    scoreboarddb.set_number_of_complete_songs(myhost,songsdb.get_number_of_downloaded_songs_from_artist(art))
    scoreboarddb.set_thread_status(myhost, 'starting artist ' + art)

    # prepare dl folder 
    scoreboarddb.set_thread_status(myhost, 'download folder ')
    os.makedirs('downloads', exist_ok=True)
    os.chdir("downloads")
    art_basename = art.split('/')[-1]
    os.makedirs(art_basename, exist_ok=True)
    os.chdir(art_basename)

    time.sleep(1.05)
    # make sure song metadata exists
    scoreboarddb.set_thread_status(myhost, 'song_metadata')
    print(myhost,'make sure we have songs')
    number_of_total_songs = len(get_undownloaded_songs(art))
    scoreboarddb.set_number_of_total_songs(myhost, number_of_total_songs)
    print(myhost, ' we have ',number_of_total_songs, ' songs' )

    time.sleep(0.05)
    # download all the songs
    print(myhost,' downloading songs')
    scoreboarddb.set_thread_status(myhost, 'begining download')
    for song in songsdb.get_all_undownloaded_songs(art):
        time.sleep(0.05)
        # sanity check on this song
        scoreboarddb.set_thread_status(myhost, 'thinking')
        mutex.acquire(1)
        if not songsdb.is_song_timestamp_old(song):
            print(myhost, ' skipping song that was touched recently')
            mutex.release()
            continue
        mutex.release()

        scoreboarddb.checkin(myhost)
        scoreboarddb.set_current_song(myhost, song)
        time.sleep(0.05)
        try:
            scoreboarddb.set_thread_status(myhost, 'downloading song')
            dl_success = yt.proxy_download(song,'socks5://127.0.0.1:'+proxydb.get_port_for_host(myhost))
            if dl_success:
                scoreboarddb.set_number_of_complete_songs(myhost,songsdb.get_number_of_downloaded_songs_from_artist(art))
                scoreboarddb.set_thread_status(myhost, 'downloaded song')

                mutex.acquire(1)
                songsdb.mark_song_downloaded(song)
                mutex.release()

                scoreboarddb.checkin(myhost)
            else:
                sb_current_song = scoreboarddb.get_current_song(myhost)
                scoreboarddb.set_thread_status(myhost, 'song download failed: ' + sb_current_song )

        except Exception as e:
            print("download exception  " + str(e))
            sb_current_song = scoreboarddb.get_current_song(myhost)
            scoreboarddb.set_thread_status(myhost, 'song download failed: ' + sb_current_song )

        time.sleep(0.05)
        scoreboarddb.set_thread_status(myhost, 'reporting')
        scoreboarddb.set_artist(myhost,'na')
        mutex.acquire(1)
        songsdb.set_song_timestamp(song)
        songsdb.increment_song_counter(song)
        mutex.release()

        scoreboarddb.set_thread_status(myhost, 'sleeping')

        print(myhost, " song downloading ok. sleeping...")
        time.sleep(3*60)

    scoreboarddb.set_thread_status(myhost, 'end of dl bookkeeping')
    time.sleep(0.05)
    mutex.acquire(1)
    artistsdb.mark_artist_downloaded(art)
    artistsdb.unclaim_artist(art)
    proxydb.unclaim_host(myhost)
    mutex.release()
    time.sleep(0.05)

def main_thread(myhost):
    scoreboarddb.register(myhost)
    scoreboarddb.set_thread_status(myhost, 'starting')
    while True: 
        print(myhost, 'main_thread, checkin')
        scoreboarddb.checkin(myhost)
        print(myhost, 'main_thread, set status')
        scoreboarddb.set_thread_status(myhost, 'main loop')
        mutex.acquire(1)
        art = artistsdb.get_undownloaded_unmarked_artists()[0]
        print('main_thread with: ', myhost, ' art: ', art)
        mutex.release()

        time.sleep(0.5)
        downloader(myhost,art)

############################################################################################################
# operation
############################################################################################################

########################## startup cleanup
# unclaim all artists
print('✅ unclaiming all artists ')
artists = artistsdb.get_all_artists()
for art in artists:
    artistsdb.unclaim_artist(art)

## undownload all artists
#print('✅ undownloading all artists 🎤🎤')
#for art in artistsdb.get_all_artists():
#    artistsdb.set_artist_flags(art, {'downloaded':False} )

# unclaim all hosts
print('✅ unclaiming all hosts')
for host in proxydb.get_all_hosts():
    proxydb.unclaim_host(host)

######################### thread start
# pick an artist
#print('✅ picking artist 🎤🎤')

## TODO i think ill need to blacklist the drum and base artist bc i dont think its real - the song urls are api calls and theres 500 million songs that come out of it
#art = artistsdb.get_undownloaded_unmarked_artists()[0]
#art = artistsdb.get_undownloaded_unmarked_artists()[1]
#artistsdb.claim_artist(art)
#print("✅✅ downloading artist: ", art)

## setup proxy
#myhost = proxydb.get_next_unclaimed_host()
##myhost = 'pibravo'
#proxydb.claim_host(myhost)
#print("✅ using host: ", myhost)
#host_port = proxydb.get_port_for_host(myhost)
#proxy_string='socks5://127.0.0.1:'+host_port

mutex = threading.Lock()
threads = []
for myhost in proxydb.get_all_hosts():
    #thread(myhost, art)
    x = threading.Thread(target=main_thread, args=(myhost,))
    x.name = 'Thread_' + myhost
    x.start()
    threads.append(x)
    time.sleep(0.8)
    #if len(threads) >=2:
    #    time.sleep(99999) # stop after first thread
    
# do admin helper tasks here
time.sleep(99999)

