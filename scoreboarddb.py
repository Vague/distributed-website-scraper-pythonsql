# scoreboarddb. track thread status

#def get_artist(host):
#def set_artist(host, song):
#def get_current_song(host):
#def set_current_song(host, song):
#def get_number_of_complete_songs(host):
#def set_number_of_complete_songs(host,num):
#def get_number_of_total_songs(host):
#def set_number_of_total_songs(host,num):
#def checkin(host):
#def get_host_dict(host):
#def get_last_checkin_time(host):
#def register(host):

import sqlite3
import os # (for getpid, remove old scoreboard.db)
import time
import json # for flags
from threading import Lock

sbdbfile = 'pathtodb.db'
os.remove(sbdbfile)
connection = sqlite3.connect(sbdbfile, check_same_thread=False)
cursor = connection.cursor()

scoreboard_mutex = Lock()
#cursor.execute("CREATE TABLE IF NOT EXISTS threads ( host TEXT primary key , start_timestamp TEXT, check_in_timestamp TEXT,  artist TEXT , number_of_total_songs TEXT, number_of_complete_songs TEXT, current_song TEXT, flags TEXT)" )
cursor.execute("CREATE TABLE IF NOT EXISTS threads ( host TEXT primary key , start_timestamp TEXT, check_in_timestamp TEXT,  artist TEXT , number_of_total_songs TEXT, number_of_complete_songs TEXT, current_song TEXT, flags TEXT, status TEXT)" )

connection.close()

# add self to scoreboard
# last checkin time
# checkin to scoreboard function
# progress = how many songs are downloaded in this artist
# update progress
# update total number of songs
# what artist am i on
# update artist

def get_thread_status(host):
    print(host, ' get_thread_status')
    return get_host_dict(host)['status']

def set_thread_status(host,status):
    print(host, ' set_thread_status')
    scoreboard_mutex.acquire(True)
    print(host, ' set_thread_status open connection')
    time.sleep(0.25)
    try:
        connection = sqlite3.connect(sbdbfile, check_same_thread=False)
        print(host, ' set_thread_status cursor')
        cursor = connection.cursor()
        print('scoreboarddb.set_status ' + host + ' ' + status )
        res = cursor.execute("update threads set status  = '" + status + "' where host = '" + host + "';")
        connection.commit()
    except Exception as e:
        print('python sucks ',host, 'stat ', status )
        print('execption ', str(e))
        time.sleep(1)
        connection.close()
            

    scoreboard_mutex.release()
    #return get_host_dict(host)['status']

def get_artist(host):
    print(host, ' get_artist')
    return get_host_dict(host)['artist']

def set_artist(host, artist):
    print(host, ' set_artist')
    scoreboard_mutex.acquire(True)
    
    print(host, ' set_artist, open connection')
    connection = sqlite3.connect(sbdbfile, check_same_thread=False)

    print(host, ' set_artist, cursor ')
    cursor = connection.cursor()

    print('scoreboarddb.set_artist ' + host + ' ' + artist )
    res = cursor.execute("update threads set artist  = '" + artist + "' where host = '" + host + "';")

    connection.commit()
    connection.close()
    scoreboard_mutex.release()
    #return get_host_dict(host)['artist']

def get_current_song(host):
    print(host, ' get_current_song')
    return get_host_dict(host)['current_song']

def set_current_song(host, song):
    print(host, ' set_current_song')
    scoreboard_mutex.acquire(True)
    connection = sqlite3.connect(sbdbfile, check_same_thread=False)
    cursor = connection.cursor()

    res = cursor.execute("update threads set current_song  = '" + song + "' where host = '" + host + "';")

    connection.commit()
    connection.close()
    scoreboard_mutex.release()
    return get_host_dict(host)['current_song']

def get_number_of_complete_songs(host):
    print(host, 'get_number_of_complete_songs')
    return get_host_dict(host)['number_of_complete_songs']

def set_number_of_complete_songs(host,num):
    print(host, 'set_number_of_complete_songs')
    scoreboard_mutex.acquire(True)
    connection = sqlite3.connect(sbdbfile, check_same_thread=False)
    cursor = connection.cursor()

    res = cursor.execute("update threads set number_of_complete_songs  = '" + str(num) + "' where host = '" + host + "';")

    connection.commit()
    connection.close()
    scoreboard_mutex.release()
    return get_host_dict(host)['number_of_complete_songs']

def get_number_of_total_songs(host):
    print(host,' get_number_of_total_songs' )
    return get_host_dict(host)['number_of_total_songs']

def set_number_of_total_songs(host,num):
    print(host, ' set_number_of_total_songs')
    scoreboard_mutex.acquire(True)
    connection = sqlite3.connect(sbdbfile, check_same_thread=False)
    cursor = connection.cursor()

    print('scoreboarddb.set_number_of_total_songs ' + host + ' ' + str(num) )
    res = cursor.execute("update threads set number_of_total_songs  = '" + str(num) + "' where host = '" + host + "';")

    connection.close()
    scoreboard_mutex.release()
    return get_host_dict(host)['number_of_total_songs']

def checkin(host):
    print(host, "checkin " + host)
    scoreboard_mutex.acquire(True)
    connection = sqlite3.connect(sbdbfile, check_same_thread=False)
    cursor = connection.cursor()

#sqlite> update artists set claimed = 'true' where name = 'artist' ;
    res = cursor.execute("update threads set check_in_timestamp = '" + str(int(time.time()))  + "' where host = '" + host + "';")

    connection.commit()
    connection.close()
    scoreboard_mutex.release()
    return get_host_dict(host)['check_in_timestamp']

def get_host_dict(host):
    print(host, 'get host dict')
    scoreboard_mutex.acquire(True)

    connection = sqlite3.connect(sbdbfile, check_same_thread=False)
    cursor = connection.cursor()

    res = cursor.execute("select * from threads where host = '"+host+"' ;")
    res = res.fetchall()[0]
    d = {}
    d['host'] = res[0]
    d['start_timestamp'] = res[1]
    d['check_in_timestamp'] = res[2]
    d['artist'] = res[3]
    d['number_of_total_songs'] = res[4]
    d['number_of_complete_songs'] = res[5]
    d['current_song'] = res[6]
    d['flags'] = res[7]
    d['status'] = res[8]

    time.sleep(0.25)
    connection.close()
    scoreboard_mutex.release()
    return d

def get_last_checkin_time(host):
    print(host, 'get_last_checkin_time')
    scoreboard_mutex.acquire(True)
    connection = sqlite3.connect(sbdbfile, check_same_thread=False)
    cursor = connection.cursor()

    res = cursor.execute("select check_in_timestamp from threads where host = '"+host+"' ;")
    res = res.fetchall()[0][0]

    connection.close()
    scoreboard_mutex.release()
    return res

def register(host):
    print(host, ' register' )
    scoreboard_mutex.acquire(True)
    print(host, ' open connection to db' )
    connection = sqlite3.connect(sbdbfile, check_same_thread=False)
    cursor = connection.cursor()

    start_timestamp = str(int(time.time())) 
    check_in_timestamp = start_timestamp
    artist = "na"
    number_of_total_songs = -1
    number_of_complete_songs = -1
    current_song = "na"
    flags = {}
    flags = json.dumps(flags)
    status = 'registered'

#cursor.execute("CREATE TABLE IF NOT EXISTS threads ( host TEXT primary key , start_timestamp TEXT, check_in_timestamp  TEXT, artist TEXT , number_of_total_songs TEXT, number_of_complete_songs TEXT, current_song TEXT, flags TEXT)" )
    sqlite_insert_with_param = """INSERT INTO threads
                          (host, start_timestamp, check_in_timestamp, artist, number_of_total_songs, number_of_complete_songs, current_song, flags, status)
                          VALUES (?, ?, ?, ?, ?, ?,?,?,?);"""
    data_tuple = (host, start_timestamp, check_in_timestamp, artist, number_of_total_songs, number_of_complete_songs, current_song, flags, status)

    cursor.execute(sqlite_insert_with_param, data_tuple)

    print(host, ' register commit')
    connection.commit()
    print(host, ' register close')
    connection.close()
    print(host, ' register mutex release')
    scoreboard_mutex.release()




#    try:
#        scoreboard_mutex.acquire()
#        connection.commit()
#        scoreboard_mutex.release()
#        print("scoreboard register succeeded for " + host )
#        return True
#    except Exception as e:
#        print("exception in scoreboard register: " + str(e))
#        return False



#def get_next_unclaimed_host():
#    # return string artist url
#    res = cursor.execute("select host from proxy where userpid = 'na' limit 1;")
#    res = res.fetchall()[0][0]
#    #print(res)
#    #print(type(res))
#    return res
#
#def claim_host(host):
#    # return boolean for success
#    print("claiming proxy " + host)
##sqlite> update artists set claimed = 'true' where name = 'artist' ;
#    res = cursor.execute("update proxy set userpid = '" + str(os.getpid())  + "' where host = '" + host + "';")
#    connection.commit()
#
#def unclaim_host(host):
#    # return boolean for success
#    print("unclaiming proxy " + host)
##sqlite> update artists set claimed = 'true' where name = 'artist' ;
#    res = cursor.execute("update proxy set userpid = 'na' where host = '" + host + "';")
#    connection.commit()
#
#def get_port_for_host(host):
#    res = cursor.execute("select port from proxy where host = '" + host + "' ;")
#    res = res.fetchall()[0][0]
#    #print(res)
#    #print(type(res))
#    return res
#
#def get_all_hosts():
#    res = cursor.execute("select host from proxy;")
#    res = res.fetchall()
#    return_list = []
#    for r in res:
#        return_list.append(r[0])
#    res = return_list
#    #print(res)
#    #print(type(res))
#    return res
