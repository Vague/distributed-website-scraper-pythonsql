#import sys # for sys.exit
#import subprocess

import json
import yt_dlp
import time # for sleep
#artist_url='artust'

print("songs mod")


# =============================================================================================
class MyLogger:
    def debug(self, msg):
        # For compatibility with youtube-dl, both debug and info are passed into debug
        # You can distinguish them by the prefix '[debug] '
        if msg.startswith('[debug] '):
            pass
        else:
            self.info(msg)

    def info(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


# ℹ️ See "progress_hooks" in help(yt_dlp.YoutubeDL)
def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now post-processing ...')
    if d['status'] == 'downloading':
        print('now we are downloading!!!')
        print('that means we probably have a speed thing',d['speed'] )
        speed = d['speed']
        print(speed/(1024**2 ),"megabytes/sec")
        print('filename', d['filename'])
    if d['status'] == 'error':
        print('we are status error!')


# =============================================================================================
def download(URLS):
    return proxy_download(URLS, proxy='none')

def get_songs( artist_url ):
    return proxy_get_songs(artist_url, proxy='none')

def proxy_get_songs( artist_url, proxy ):
    # returns list of strings = song urls

    #  See help(yt_dlp.YoutubeDL) for a list of available options and public functions
    ydl_opts = {
    'ignoreerrors':'true',
    }

    if proxy != "none":
        ydl_opts['proxy'] = proxy

    songs = []
    print("get songs from " + artist_url)
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(artist_url, download=False)

        # ℹ️ ydl.sanitize_info makes the info json-serializable
        res = json.dumps(ydl.sanitize_info(info))
        print(res)
    json_result = json.loads(res)
    for item in json_result['entries']:
        song = item['webpage_url']
        songs.append(song)

    return songs

def proxy_download(URLS,proxy):
    # ratelimit is in bytes, 2000 comes out as 2KiB/s
    ydl_opts = {
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
    'ratelimit' : 200000,
    'format': 'aac/bestaudio/best',
    # ℹ️ See help(yt_dlp.postprocessor) for a list of available Postprocessors and their arguments
    'postprocessors': [{  # Extract audio using ffmpeg
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'aac',
    }]
    }

    if proxy != "none":
        ydl_opts['proxy'] = proxy

    # URLS takes a list of strings, or a single string. it probably takes more?
    
    try: 
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            error_code = ydl.download(URLS)
        return True
    except Exception as e:
        print('exception: ', e )
        return False
    

