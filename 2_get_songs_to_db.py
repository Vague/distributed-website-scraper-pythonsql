# get all songs from all artists and put them into a huge songs table

############################################################################################################
# setup script
############################################################################################################

import yt,proxydb
import artistsdb
import songsdb
import os,time

############################################################################################################
# operation
############################################################################################################

########################## cleanup
## unclaim all artists
#print('✅ unclaiming all artists ')
#artists = artistsdb.get_all_artists()
#for art in artists:
#    artistsdb.unclaim_artist(art)
#
### undownload all artists
##print('✅ undownloading all artists 🎤🎤')
##for art in artistsdb.get_all_artists():
##    artistsdb.set_artist_flags(art, {'downloaded':False} )
#
## unclaim all hosts
#print('✅ unclaiming all hosts')
#for host in proxydb.get_all_hosts():
#    proxydb.unclaim_host(host)
#
######################### thread start
# pick an artist
print('✅ picking artist 🎤🎤')

# TODO i think ill need to blacklist the drum and base artist bc i dont think its real - the song urls are api calls and theres 500 million songs that come out of it 
art = artistsdb.get_undownloaded_unmarked_artists()[0]
art = artistsdb.get_undownloaded_unmarked_artists()[1]
artistsdb.claim_artist(art)
print("✅✅ downloading artist: ", art)

# setup proxy
myhost = proxydb.get_next_unclaimed_host()
#myhost = 'myhostb'
proxydb.claim_host(myhost)
print("✅ using host: ", myhost)
host_port = proxydb.get_port_for_host(myhost)
proxy_string='socks5://127.0.0.1:'+host_port


# download all the songs
songs = songsdb.get_all_undownloaded_songs(art)
if len(songs) == 0:
    print("there are no valid songs, fetching...")
    #songs = yt.get_songs(art)
    songs = yt.proxy_get_songs(art, proxy_string)
    for song in songs:
        songsdb.insert_song_metadata(art, song)
        songsdb.mark_song_undownloaded(song)

# download the artist's songs
# this should probably be a function
# it needs to handle claiming, retries, failures, etc
print("☠️  ☠️  ☠️  starting download !!! " )
artistsdb.claim_artist(art)
# prepare dl folder 
os.makedirs('downloads', exist_ok=True)
os.chdir("downloads")
art_basename = art.split('/')[-1]
os.makedirs(art_basename, exist_ok=True)
os.chdir(art_basename)
# download all the songs
for song in songsdb.get_all_undownloaded_songs(art):
    yt.proxy_download(song,'socks5://127.0.0.1:'+proxydb.get_port_for_host(myhost))
    songsdb.mark_song_downloaded(song)
    songsdb.increment_song_counter(song)
    songsdb.set_song_timestamp(song)
    print("song downloading ok. sleeping...")
    time.sleep(3*60)
artistsdb.mark_artist_downloaded(art)
artistsdb.unclaim_artist(art)
proxydb.unclaim_host(myhost)



